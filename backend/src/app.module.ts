import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BuildUserProfileModule } from './module/UserProfile.module';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule } from '@nestjs/config';

ConfigModule.forRoot();

@Module({
  imports: [
    BuildUserProfileModule,
    MongooseModule.forRoot(process.env.MONGO_BD_URI),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
