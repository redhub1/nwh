import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { LinkedInProfileScraper } from 'linkedin-test-scraper';
import { ProfileEntity } from 'src/models/profile.entity';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class UserProfileService {
  constructor(
    @InjectModel('Profile')
    private readonly ProfileModel: Model<ProfileEntity>,
  ) {}
  async buildUserProfile(profileLink: string, cookie: string): Promise<object> {
    const isStored = await this.ProfileModel.findOne({
      'user.url': { $eq: profileLink },
    }).exec();

    const now = Date.now();

    if (isStored) {
      await this.ProfileModel.updateOne(
        {
          _id: { $eq: isStored._id },
        },
        { $set: { requestedAt: now } },
      ).exec();
      return isStored;
    }

    const scraper = new LinkedInProfileScraper({
      sessionCookieValue: cookie,
      timeout: 20000,
      keepAlive: false,
    });
    await scraper.setup();
    const {
      userProfile,
      experiences,
      volunteerExperiences,
      skills,
      education,
      rawRelatedSearches,
    } = await scraper.run(profileLink);

    const profileModel = new this.ProfileModel({
      user: userProfile,
      experiences: experiences,
      education: education,
      skills: skills,
      volunteerExperiences: volunteerExperiences,
      relatedSearches: rawRelatedSearches,
      createdAt: now,
      requestedAt: now,
      like: false,
    });

    if (
      !userProfile ||
      (experiences.length === 0 &&
        volunteerExperiences.length === 0 &&
        skills.length === 0 &&
        education.length === 0)
    ) {
      return new BadRequestException(
        { object: 'profile', status: 'Empty' },
        'Something went wrong, Try again',
      );
    }

    return await profileModel.save();
  }

  async getHistory(historySize: number): Promise<object> {
    return await this.ProfileModel.find()
      .sort({ requestedAt: -1 })
      .limit(historySize)
      .exec();
  }

  async likeProfile(id: string): Promise<object> {
    const { matchedCount, modifiedCount } = await this.ProfileModel.updateOne(
      { _id: id },
      { $set: { like: true } },
    );

    if (matchedCount === 0) {
      return new NotFoundException(matchedCount, 'Profile Not Found');
    }

    if (modifiedCount === 1) {
      return this.ProfileModel.find({ _id: id });
    }

    return { message: 'Not Modified', error: 304 };
  }
}
