import { Controller, Get, Param, Post, Query } from '@nestjs/common';
import { UserProfileService } from 'src/service/UserProfile.service';

@Controller('/user')
export class UserProfileController {
  constructor(private _userProfileService: UserProfileService) {}

  @Post('/profile/:profileLink/:cookie')
  async getUserProfile(
    @Param('profileLink') profileLink: string,
    @Param('cookie') cookie: string,
  ): Promise<object> {
    return this._userProfileService.buildUserProfile(profileLink, cookie);
  }

  @Get('/getHistory')
  getHistory(@Query('historySize') historySize: number): object {
    const size = historySize ?? 10;
    return this._userProfileService.getHistory(size);
  }

  @Post('/likeProfile/:id')
  likeProfile(@Param('id') id: string): object {
    return this._userProfileService.likeProfile(id);
  }
}
