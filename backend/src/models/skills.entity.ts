export class SkillsEntity {
  get skillName(): string {
    return this._skillName;
  }

  get endorsementCount(): number {
    return this._endorsementCount;
  }
  constructor(private _skillName: string, private _endorsementCount: number) {}
}
