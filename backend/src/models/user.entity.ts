export class UserEntity {
  get title(): string {
    return this._title;
  }
  get id(): number {
    return this._id;
  }

  get fullName(): string {
    return this._fullName;
  }

  get location(): string {
    return this._location;
  }

  get photo(): string {
    return this._photo;
  }

  get description(): string {
    return this._description;
  }

  get linkedIn(): string {
    return this._linkedIn;
  }
  constructor(
    private _id: number,
    private _fullName: string,
    private _location: string,
    private _title: string,
    private _photo: string,
    private _description: string,
    private _linkedIn: string,
  ) {}
}
