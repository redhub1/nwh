import { UserEntity } from './user.entity';
import { ExperiencesEntity } from './experiences.entity';
import { SkillsEntity } from './skills.entity';
import { EducationEntity } from './education.entity';
import * as mongoose from 'mongoose';

export const RelatedSearches = new mongoose.Schema({
  relatedName: { type: String },
  photoRelated: { type: String },
});

const VolunteerExperiences = new mongoose.Schema({
  title: String,
  company: String,
  startDate: String,
  endDate: String,
  endDateIsPresent: Boolean,
  description: String,
  durationInDays: String,
});

const LocationSchema = new mongoose.Schema({
  city: String,
  province: String,
  location: String,
});

const UserSchema = new mongoose.Schema({
  id: Number,
  fullName: String,
  location: { type: LocationSchema },
  title: String,
  photo: String,
  description: String,
  url: String,
});

const EducationSchema = new mongoose.Schema({
  schoolName: String,
  degreeName: String,
  fieldOfStudy: String,
  startDate: String,
  endDate: String,
  durationInDays: String,
});
const ExperiencesSchema = new mongoose.Schema({
  title: String,
  company: String,
  employmentType: String,
  location: { type: LocationSchema },
  startDate: String,
  endDate: String,
  edDateIsPresent: Boolean,
  description: String,
  durationInDays: String,
});
const SkillSchema = new mongoose.Schema({
  skillName: String,
  endorsementCount: Number,
});

export const ProfileSchema = new mongoose.Schema({
  user: { type: UserSchema },
  experiences: [ExperiencesSchema],
  education: [EducationSchema],
  skills: [SkillSchema],
  volunteerExperiences: [VolunteerExperiences],
  relatedSearches: [RelatedSearches],
  createdAt: { type: Date },
  requestedAt: { type: Date },
  like: { type: Boolean },
});

export interface ProfileEntity {
  _id: string;
  user: UserEntity;
  experiences: ExperiencesEntity;
  education: EducationEntity;
  skills: SkillsEntity;
  //relatedSearches: RelatedEntity,
  //volunteerExperiences: VolunteerEntity;
  createdAt: Date;
  requestedAt: Date;
  like: boolean;
}
