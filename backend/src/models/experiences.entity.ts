export class ExperiencesEntity {
  get title(): string {
    return this._title;
  }

  get company(): string {
    return this._company;
  }

  get employmentType(): string {
    return this._employmentType;
  }

  get location(): string {
    return this._location;
  }

  get startDate(): Date {
    return this._startDate;
  }

  get endDate(): Date {
    return this._endDate;
  }

  get edDateIsPresent(): boolean {
    return this._edDateIsPresent;
  }

  get description(): string {
    return this._description;
  }

  get durationInDays(): number {
    return this._durationInDays;
  }
  constructor(
    private _title: string,
    private _company: string,
    private _employmentType: string,
    private _location: string,
    private _startDate: Date,
    private _endDate: Date,
    private _edDateIsPresent: boolean,
    private _description: string,
    private _durationInDays: number,
  ) {}
}
