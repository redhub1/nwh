export class EducationEntity {
  get schoolName(): string {
    return this._schoolName;
  }

  get degreeName(): string {
    return this._degreeName;
  }

  get fieldOfStudy(): string {
    return this._fieldOfStudy;
  }

  get startDate(): Date {
    return this._startDate;
  }

  get endDate(): Date {
    return this._endDate;
  }

  get durationInDays(): number {
    return this._durationInDays;
  }
  constructor(
    private _schoolName: string,
    private _degreeName: string,
    private _fieldOfStudy: string,
    private _startDate: Date,
    private _endDate: Date,
    private _durationInDays: number,
  ) {}
}
