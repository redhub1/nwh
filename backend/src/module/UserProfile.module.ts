import { Module } from '@nestjs/common';
import { UserProfileController } from '../controller/UserProfile.controller';
import { UserProfileService } from '../service/UserProfile.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ProfileSchema } from 'src/models/profile.entity';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Profile', schema: ProfileSchema }]),
  ],
  controllers: [UserProfileController],
  providers: [UserProfileService],
  exports: [],
})
export class BuildUserProfileModule {}
