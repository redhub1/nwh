import stampit from 'stampit';
import ServiceStamp from '~/services/service';

export const ProfileServiceStamp = stampit.methods({
  getProfile(urlProfile, activeCookie) {
    urlProfile = urlProfile.replaceAll(':','%3A')
    urlProfile = urlProfile.replaceAll('/','%2F')
    return this.fetchApi.post(`profile?profileLink=${urlProfile}&cookie=${activeCookie}`);
  },
  getProfiles() {
    return this.fetchApi.get('getHistory');
  },
  updateProfile(id) {
    return this.fetchApi.post(`likeProfile/${id}`, id)
  },
});
const ProfileServices = stampit.compose(ServiceStamp, ProfileServiceStamp);
export default ProfileServices;
