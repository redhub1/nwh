import ProfileServices from "~/services/profile/profile-services";

export const state = () => ({
  profile: null,
  profiles: [],
  cookie: null,
  loading: false,
  error: null,
  url: null,
})

export const actions = {
  async getProfile ({ commit }, params){
    const service = this.$getRedHubServices(ProfileServices);
    commit('changeLoading', true);
    try {
      const result = await service.getProfile(params.url, params.cookie);
      await commit('storeProfile', result.data);
    } catch (error) {
      commit('catchError', error);
    }
    commit('changeLoading', false);
  },
  async getProfiles ({ commit }){
    const service = this.$getRedHubServices(ProfileServices);
    try {
      const result = await service.getProfiles();
      await commit('storeProfiles', result.data)
    } catch (error) {
      commit('catchError', error);
    }
  },
  async updateProfile ({ commit }, profile){
    const service = this.$getRedHubServices(ProfileServices);
    try {
      await service.updateProfile(profile._id);
    } catch (error) {
      commit('catchError', error);
    }
  },
}

export const mutations = {
  storeProfile(_state, profile) {
    if (profile.skills === undefined ) {
      _state.profile = profile;
    } else {
      if (profile.skills.length > 4) {
        profile.skills.length = 4
      }
      _state.profile = profile;
    }
  },
  storeUrl(_state, url) {
    _state.url = url;
  },
  updateLike(_state) {
    if(_state.profile !== null && _state.profile !== undefined){
      _state.profile.like = !_state.profile.like
    }
  },
  storeCookie(_state, cookie) {
    _state.cookie = cookie;
  },
  storeProfiles(_state, profiles) {
    _state.profiles = profiles;
  },
  changeLoading(_state, loading) {
    _state.loading = loading;
  },
  catchError(_state, error) {
    _state.error = error;
  },
}
